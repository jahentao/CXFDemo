package com.jahentao.ws.server;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.xml.ws.Endpoint;

import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import com.jahentao.ws.server.inter.impl.ProcessCreditCard;
// Web服务类
public class WSServlet extends CXFNonSpringServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void loadBus(ServletConfig sc) throws ServletException {
		super.loadBus(sc);
		// 发布服务
		// Endpoint 进行服务的发布，可以连续发布多个服务
		// 地址 “creditcard” 是在web.xml中url-pattern匹配完毕后剩余的url
		Endpoint.publish("/creditcard", new ProcessCreditCard());
	}
	
}
