package com.jahentao.ws.server.inter;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Web 服务接口，模拟实现电子支付功能
 * @author jahentao
 *
 */

@WebService
public interface IProcessCredit {
	/**
	 * 电子支付
	 * @param cardId 信用卡id
	 * @param total 消费金额
	 * @return 处理结果
	 */
	public int creditProcess(
			@WebParam(name="cardId") String cardId,
			@WebParam(name="total") double total);
}
