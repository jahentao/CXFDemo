package com.jahentao.ws.server.inter.impl;

import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;

import com.jahentao.ws.server.inter.IProcessCredit;
/**
 * 信用卡处理接口的实现类
 */
// endpointInterface 属性明确指出了该实现类的接口
@WebService(endpointInterface = "com.jahentao.ws.server.inter.IProcessCredit")
public class ProcessCreditCard implements IProcessCredit {

	// 存储信用卡号和余额，模拟数据库
	private static Map<String, Double> data = new HashMap<String, Double>();
	
	@Override
	public int creditProcess(String cardId, double total) {
		initData(); 
		//判断是否有此卡号
		if(!data.containsKey(cardId)) {
			return -1;
		}
		double balance = data.get(cardId); // 返回余额
		if(total < balance) {
			return 0; // 余额不足
		}
		balance = balance - total; // 减去余额
		data.put(cardId, balance); // 重新设置
		// 成功返回
		return 1;
	}

	/**
	 * 初始化Map结构
	 */
	private void initData() {
		if(data.size()==0) {
			data.put("12345", 1000d);
			data.put("23456", 1002d);
			data.put("34567", 10000d);
			data.put("45678", 1000d);
		}
	}

}
