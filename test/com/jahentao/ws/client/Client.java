package com.jahentao.ws.client;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.jahentao.ws.server.inter.IProcessCredit;

/**
 * Web服务客户端 测试
 * @author jahentao
 *
 */
public class Client {
	
	public static void main(String[] args) {
		// 创建WebService客户端代理工厂
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		// 创建Web服务接口
		factory.create(IProcessCredit.class);
		// 设置地址
		factory.setAddress("http://localhost:8080/CXFDemo/ws/creditcard");
		// 通过代理工厂返回接口的实例
		IProcessCredit client = (IProcessCredit) factory.create();
		// 传入参数
		int result = client.creditProcess("23456", 1500);
		if(result == -1) {
			System.out.println("信用卡号不正确");
		} else if(result == 0) {
			System.out.println("余额不足！");
		} else if(result == 1) {
			System.out.println("信用卡支付成功！");
		}
	}
}
