# 实验环境

- Tomcat 8.5
- JDK 1.8
- CXF 2.3.2

# 参考
《Web Service 应用开发》电子工业出版社 CXF

# 运行Demo

## 在Tomcat上运行项目CXFDemo

- 输入 http://localhost:8080/CXFDemo/ws/creditcard?wsdl 测试，正常显示如下：

![访问wsdl](http://images-markdown.oss-cn-hangzhou.aliyuncs.com/course/WebService/%E8%AE%BF%E9%97%AEcreditcard_wsdl.png)

- 输入 http://localhost:8080/CXFDemo/ws/creditcard/creditProcess?cardId=1234567&total=200 调用方法，正常显示如下：

![调用服务](http://images-markdown.oss-cn-hangzhou.aliyuncs.com/course/WebService/%E8%B0%83%E7%94%A8CXF%E6%96%B9%E6%B3%95.png)

## 运行Client

客户端测试

![客户端调用WebService](http://images-markdown.oss-cn-hangzhou.aliyuncs.com/course/WebService/%E5%AE%A2%E6%88%B7%E7%AB%AF%E7%BC%96%E7%A8%8B_%E8%B0%83%E7%94%A8WebService.png)


